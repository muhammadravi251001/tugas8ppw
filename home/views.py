from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import requests
import json

# Create your views here.

def webpage1(request):
    return render(request, 'home/pageSelamatDatang.html')

def webpage2(request):
    response = {}
    return render(request, 'home/buku.html', response)

def fungsi_data(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    ret = requests.get(url)
    data = json.loads(ret.content)
    return JsonResponse(data, safe=False)



