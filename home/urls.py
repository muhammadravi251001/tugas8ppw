from django.urls import path
from . import views
#from .views import fungsi_data

urlpatterns = [
    path('', views.webpage1, name='webpage1'),
    path('buku/', views.webpage2, name='webpage2'),
    path('data/', views.fungsi_data),
]